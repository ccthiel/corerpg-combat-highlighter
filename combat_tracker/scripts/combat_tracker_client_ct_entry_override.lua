function onInit()
	UDGCoreRPGCombatHighlighterHelper.verbose({"combat_tracker_client_override.lua::onInit"});

	if super and super.onInit then
		super.onInit();
	end
end

function onDoubleClick(x, y)
	UDGCoreRPGCombatHighlighterHelper.verbose({"combat_tracker_client_override.lua::onDoubleClick()"});
	local CombatTrackerNode = window.getDatabaseNode();
	CombatManager.openMap(CombatTrackerNode);

	if super and super.onDoubleClick then
		super.onDoubleClick(x, y);
	end
end
