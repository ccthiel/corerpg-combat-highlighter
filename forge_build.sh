#!/bin/bash

zip -r CoreRPG-Combat-Highlighter.ext . -x '.git/*' -x .gitignore -x '.vscode/*' -x .markdownlint.json -x .gitlab-ci.yml -x forge_build.sh -x LICENSE -x CoreRPG-Combat-Highlighter.code-workspace -x .gitpod.yml
