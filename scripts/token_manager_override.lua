wUniHover = nil;

function onInit()
	UDGCoreRPGCombatHighlighterHelper.verbose({"token_manager_override.lua::onInit()"});

	originalOnHover = Token.onHover;
	Token.onHover = onHover;

	originalOnDelete = Token.onDelete;
	Token.onDelete = onDelete;
end

function onHover(tokenMap, bOver)
	UDGCoreRPGCombatHighlighterHelper.verbose({"token_manager_override.lua::onHover(tokenMap, bOver)", "tokenMap: ", tokenMap, "bOver: ", bOver});

	if OptionsManager.isOption("CORERPG_COMBAT_HIGHLIGHTER_DISABLE_FUNCTIONALITY_WHEN_DRAGGING", "on") and Input.getDragData() then
		-- Do nothing - we're dragging
	else
		local ceWindow = nil;
		if tokenMap then
			ceWindow = UDGCoreRPGCombatHighlighterCombatManagerHelper.getCombatEntryWindowFromToken(tokenMap);
		end
	
		local combatTrackerWindow = nil;
		if Session.IsHost then
			combatTrackerWindow = Interface.findWindow("combattracker_host", "combattracker");
		else
			combatTrackerWindow = Interface.findWindow("combattracker_client", "combattracker");
		end
	
		local windowList = nil;
		if combatTrackerWindow then 
			windowList = combatTrackerWindow.list;
		end
	
		if OptionsManager.isOption("CORERPG_COMBAT_HIGHLIGHTER_TOKEN_HOVER_ON_HIGHLIGHT_CT_ENTRY", "on") then
			if ceWindow then
				if bOver then
					if wUniHover and wUniHover ~= ceWindow then
						wUniHover.updateDisplay();
					end
					ceWindow.setFrame("ctentrybox_highlight");
				else
					ceWindow.updateDisplay();
				end
			end
		end
	
		if OptionsManager.isOption("CORERPG_COMBAT_HIGHLIGHTER_TOKEN_HOVER_ON_AUTO_SCROLL_TO_CT_ENTRY", "on") then
			if windowList and bOver and ceWindow then
				windowList.scrollToWindow(ceWindow);
				wUniHover = ceWindow;
			end
		end
	
		if OptionsManager.isOption("CORERPG_COMBAT_HIGHLIGHTER_TOKEN_HOVER_OFF_AUTO_SCROLL_TO_ACTIVE_CT_ENTRY", "on") then
			local bDualHover;
			if wUniHover and wUniHover ~= ceWindow then
				bDualHover = true;
			end
			if ((not bOver) or bDualHover) and windowList then
				local node = CombatManager.getActiveCT();
				local activeCE = nil;
	
				if node then
					activeCE = UDGCoreRPGCombatHighlighterCombatManagerHelper.getCombatEntryWindowFromNode(node);
				end
	
				if activeCE then
					windowList.scrollToWindow(activeCE);
				end
			end
		end

		if bOver and ceWindow then
			wUniHover = ceWindow;
		else
			if not bOver and wUniHover and ceWindow == wUniHover then
				wUniHover = nil;
			end
		end
	end

	if originalOnHover then
		originalOnHover(tokenMap, bOver);
	end
end

function onDelete(token)
	UDGCoreRPGCombatHighlighterHelper.verbose({"token_manager_override.lua::onDelete(token)", "token: ", token});

	combatants = CombatManager.getCombatantNodes();

	for _,combatant in pairs(combatants) do
		local window = UDGCoreRPGCombatHighlighterCombatManagerHelper.getCombatEntryWindowFromNode(combatant); 

		if window then
			window.updateDisplay();
		end
	end

	if originalOnDelete then
		originalOnDelete(token);
	end
end
